/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TIMER_TIME_H
#define TIMER_TIME_H

#include <stdint.h>

//=============================================================================
// Public function declarations
//=============================================================================
void time_start(void);
uint32_t millis(void);

#endif /* TIMER_TIME_H */

