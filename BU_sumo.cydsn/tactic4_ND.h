/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef TACTIC_4_H
#define TACTIC_4_H

    
typedef enum {
  DRIVE_STOP,
  DRIVE_FORWARD,
  DRIVE_BACKWARD,
  DRIVE_TURN_LEFT,
  DRIVE_TURN_RIGHT
} DriveDirections_t;    
//=============================================================================
// Public function declarations
//=============================================================================
void RunTactic4(void);

#endif /* TACTIC1_H */
/* [] END OF FILE */
