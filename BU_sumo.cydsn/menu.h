/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "stdio.h"
#include "button_input.h"

#define MENU_EVENT_IDLE    0xFF
#define MENU_EVENT_EXIT    0xFE

#define MENU_KEYCODE_DOWN	13

#define MENU_KEYCODE_UP		12

#define MENU_KEYCODE_ENTER	11

struct Menu {
	const char* title;
	const char* *strings;
	uint8_t strings_cnt;
	uint8_t current_selection;
	uint8_t current_page;
};
typedef struct Menu Menu;


//! Masks for robot buttons ( FL BL BR FR )
enum buttons {
    Button_Encoder  =   0b00010000,
    Button_Up =         0b00001000,
    Button_Right =      0b00000100,
	Button_Down=        0b00000010,
	Button_Left =       0b00000001    
};


void showText(int x, char* text);
void mainLogo(void);
int ShowMenu(Menu *menu);
void showNumber(int number);
int getButoonState(int cursor, int num_elements);
void opponentSensors(void);
void lineSensors(void);
void GoTo(int content);
/* [] END OF FILE */
