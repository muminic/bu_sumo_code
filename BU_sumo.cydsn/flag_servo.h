/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef SERVO_H
#define SERVO_H

#include "project.h"
    
void flagServo_Init(void);
void servoAngle(int angle);  
void servoRightAngle(int angle);
void rightFlagUp(void);
void rightFlagDown(void);
void leftFlagUp(void);
void leftFlagDown(void);
    
#endif /* SERVO_H */
/* [] END OF FILE */
