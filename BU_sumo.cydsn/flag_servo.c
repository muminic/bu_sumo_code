/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "flag_servo.h"

void flagServo_Init(){
    Servo_Start();
    Servo_1_Start();
    
}

void servoAngle(int angle){
    if (angle >= 180) {angle = 180;}
    int pulseWidth = 300 + (angle * 5.6); // 1649  //0deg = 300, 180deg = 1300  period 20ms    
    Servo_WriteCompare(pulseWidth);
}

void servoRightAngle(int angle){
    if (angle >= 180) {angle = 180;}
    int pulseWidth = 300 + (angle * 5.6); // 1649  //0deg = 300, 180deg = 1300  period 20ms    
    Servo_1_WriteCompare(pulseWidth);
}

void rightFlagUp(void){
    servoRightAngle(125);
}
void rightFlagDown(void){
    servoRightAngle(35);
}
void leftFlagUp(void){
    servoAngle(20);
}
void leftFlagDown(void){
    servoAngle(105);
}

//CyDelay(1000);
 //   servoAngle(20); 
    
  //  CyDelay(1000);
  //  servoAngle(105);
  //  servoRightAngle(35);


//void flagDown(void){
    //servoAngle(90);    
//}
/* [] END OF FILE */
