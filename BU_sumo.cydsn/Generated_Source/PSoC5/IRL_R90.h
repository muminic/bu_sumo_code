/*******************************************************************************
* File Name: IRL_R90.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_IRL_R90_H) /* Pins IRL_R90_H */
#define CY_PINS_IRL_R90_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "IRL_R90_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 IRL_R90__PORT == 15 && ((IRL_R90__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    IRL_R90_Write(uint8 value);
void    IRL_R90_SetDriveMode(uint8 mode);
uint8   IRL_R90_ReadDataReg(void);
uint8   IRL_R90_Read(void);
void    IRL_R90_SetInterruptMode(uint16 position, uint16 mode);
uint8   IRL_R90_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the IRL_R90_SetDriveMode() function.
     *  @{
     */
        #define IRL_R90_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define IRL_R90_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define IRL_R90_DM_RES_UP          PIN_DM_RES_UP
        #define IRL_R90_DM_RES_DWN         PIN_DM_RES_DWN
        #define IRL_R90_DM_OD_LO           PIN_DM_OD_LO
        #define IRL_R90_DM_OD_HI           PIN_DM_OD_HI
        #define IRL_R90_DM_STRONG          PIN_DM_STRONG
        #define IRL_R90_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define IRL_R90_MASK               IRL_R90__MASK
#define IRL_R90_SHIFT              IRL_R90__SHIFT
#define IRL_R90_WIDTH              1u

/* Interrupt constants */
#if defined(IRL_R90__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in IRL_R90_SetInterruptMode() function.
     *  @{
     */
        #define IRL_R90_INTR_NONE      (uint16)(0x0000u)
        #define IRL_R90_INTR_RISING    (uint16)(0x0001u)
        #define IRL_R90_INTR_FALLING   (uint16)(0x0002u)
        #define IRL_R90_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define IRL_R90_INTR_MASK      (0x01u) 
#endif /* (IRL_R90__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define IRL_R90_PS                     (* (reg8 *) IRL_R90__PS)
/* Data Register */
#define IRL_R90_DR                     (* (reg8 *) IRL_R90__DR)
/* Port Number */
#define IRL_R90_PRT_NUM                (* (reg8 *) IRL_R90__PRT) 
/* Connect to Analog Globals */                                                  
#define IRL_R90_AG                     (* (reg8 *) IRL_R90__AG)                       
/* Analog MUX bux enable */
#define IRL_R90_AMUX                   (* (reg8 *) IRL_R90__AMUX) 
/* Bidirectional Enable */                                                        
#define IRL_R90_BIE                    (* (reg8 *) IRL_R90__BIE)
/* Bit-mask for Aliased Register Access */
#define IRL_R90_BIT_MASK               (* (reg8 *) IRL_R90__BIT_MASK)
/* Bypass Enable */
#define IRL_R90_BYP                    (* (reg8 *) IRL_R90__BYP)
/* Port wide control signals */                                                   
#define IRL_R90_CTL                    (* (reg8 *) IRL_R90__CTL)
/* Drive Modes */
#define IRL_R90_DM0                    (* (reg8 *) IRL_R90__DM0) 
#define IRL_R90_DM1                    (* (reg8 *) IRL_R90__DM1)
#define IRL_R90_DM2                    (* (reg8 *) IRL_R90__DM2) 
/* Input Buffer Disable Override */
#define IRL_R90_INP_DIS                (* (reg8 *) IRL_R90__INP_DIS)
/* LCD Common or Segment Drive */
#define IRL_R90_LCD_COM_SEG            (* (reg8 *) IRL_R90__LCD_COM_SEG)
/* Enable Segment LCD */
#define IRL_R90_LCD_EN                 (* (reg8 *) IRL_R90__LCD_EN)
/* Slew Rate Control */
#define IRL_R90_SLW                    (* (reg8 *) IRL_R90__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define IRL_R90_PRTDSI__CAPS_SEL       (* (reg8 *) IRL_R90__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define IRL_R90_PRTDSI__DBL_SYNC_IN    (* (reg8 *) IRL_R90__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define IRL_R90_PRTDSI__OE_SEL0        (* (reg8 *) IRL_R90__PRTDSI__OE_SEL0) 
#define IRL_R90_PRTDSI__OE_SEL1        (* (reg8 *) IRL_R90__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define IRL_R90_PRTDSI__OUT_SEL0       (* (reg8 *) IRL_R90__PRTDSI__OUT_SEL0) 
#define IRL_R90_PRTDSI__OUT_SEL1       (* (reg8 *) IRL_R90__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define IRL_R90_PRTDSI__SYNC_OUT       (* (reg8 *) IRL_R90__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(IRL_R90__SIO_CFG)
    #define IRL_R90_SIO_HYST_EN        (* (reg8 *) IRL_R90__SIO_HYST_EN)
    #define IRL_R90_SIO_REG_HIFREQ     (* (reg8 *) IRL_R90__SIO_REG_HIFREQ)
    #define IRL_R90_SIO_CFG            (* (reg8 *) IRL_R90__SIO_CFG)
    #define IRL_R90_SIO_DIFF           (* (reg8 *) IRL_R90__SIO_DIFF)
#endif /* (IRL_R90__SIO_CFG) */

/* Interrupt Registers */
#if defined(IRL_R90__INTSTAT)
    #define IRL_R90_INTSTAT            (* (reg8 *) IRL_R90__INTSTAT)
    #define IRL_R90_SNAP               (* (reg8 *) IRL_R90__SNAP)
    
	#define IRL_R90_0_INTTYPE_REG 		(* (reg8 *) IRL_R90__0__INTTYPE)
#endif /* (IRL_R90__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_IRL_R90_H */


/* [] END OF FILE */
