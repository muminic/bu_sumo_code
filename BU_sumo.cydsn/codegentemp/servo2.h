/*******************************************************************************
* File Name: servo2.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_servo2_H) /* Pins servo2_H */
#define CY_PINS_servo2_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "servo2_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 servo2__PORT == 15 && ((servo2__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    servo2_Write(uint8 value);
void    servo2_SetDriveMode(uint8 mode);
uint8   servo2_ReadDataReg(void);
uint8   servo2_Read(void);
void    servo2_SetInterruptMode(uint16 position, uint16 mode);
uint8   servo2_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the servo2_SetDriveMode() function.
     *  @{
     */
        #define servo2_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define servo2_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define servo2_DM_RES_UP          PIN_DM_RES_UP
        #define servo2_DM_RES_DWN         PIN_DM_RES_DWN
        #define servo2_DM_OD_LO           PIN_DM_OD_LO
        #define servo2_DM_OD_HI           PIN_DM_OD_HI
        #define servo2_DM_STRONG          PIN_DM_STRONG
        #define servo2_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define servo2_MASK               servo2__MASK
#define servo2_SHIFT              servo2__SHIFT
#define servo2_WIDTH              1u

/* Interrupt constants */
#if defined(servo2__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in servo2_SetInterruptMode() function.
     *  @{
     */
        #define servo2_INTR_NONE      (uint16)(0x0000u)
        #define servo2_INTR_RISING    (uint16)(0x0001u)
        #define servo2_INTR_FALLING   (uint16)(0x0002u)
        #define servo2_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define servo2_INTR_MASK      (0x01u) 
#endif /* (servo2__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define servo2_PS                     (* (reg8 *) servo2__PS)
/* Data Register */
#define servo2_DR                     (* (reg8 *) servo2__DR)
/* Port Number */
#define servo2_PRT_NUM                (* (reg8 *) servo2__PRT) 
/* Connect to Analog Globals */                                                  
#define servo2_AG                     (* (reg8 *) servo2__AG)                       
/* Analog MUX bux enable */
#define servo2_AMUX                   (* (reg8 *) servo2__AMUX) 
/* Bidirectional Enable */                                                        
#define servo2_BIE                    (* (reg8 *) servo2__BIE)
/* Bit-mask for Aliased Register Access */
#define servo2_BIT_MASK               (* (reg8 *) servo2__BIT_MASK)
/* Bypass Enable */
#define servo2_BYP                    (* (reg8 *) servo2__BYP)
/* Port wide control signals */                                                   
#define servo2_CTL                    (* (reg8 *) servo2__CTL)
/* Drive Modes */
#define servo2_DM0                    (* (reg8 *) servo2__DM0) 
#define servo2_DM1                    (* (reg8 *) servo2__DM1)
#define servo2_DM2                    (* (reg8 *) servo2__DM2) 
/* Input Buffer Disable Override */
#define servo2_INP_DIS                (* (reg8 *) servo2__INP_DIS)
/* LCD Common or Segment Drive */
#define servo2_LCD_COM_SEG            (* (reg8 *) servo2__LCD_COM_SEG)
/* Enable Segment LCD */
#define servo2_LCD_EN                 (* (reg8 *) servo2__LCD_EN)
/* Slew Rate Control */
#define servo2_SLW                    (* (reg8 *) servo2__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define servo2_PRTDSI__CAPS_SEL       (* (reg8 *) servo2__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define servo2_PRTDSI__DBL_SYNC_IN    (* (reg8 *) servo2__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define servo2_PRTDSI__OE_SEL0        (* (reg8 *) servo2__PRTDSI__OE_SEL0) 
#define servo2_PRTDSI__OE_SEL1        (* (reg8 *) servo2__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define servo2_PRTDSI__OUT_SEL0       (* (reg8 *) servo2__PRTDSI__OUT_SEL0) 
#define servo2_PRTDSI__OUT_SEL1       (* (reg8 *) servo2__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define servo2_PRTDSI__SYNC_OUT       (* (reg8 *) servo2__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(servo2__SIO_CFG)
    #define servo2_SIO_HYST_EN        (* (reg8 *) servo2__SIO_HYST_EN)
    #define servo2_SIO_REG_HIFREQ     (* (reg8 *) servo2__SIO_REG_HIFREQ)
    #define servo2_SIO_CFG            (* (reg8 *) servo2__SIO_CFG)
    #define servo2_SIO_DIFF           (* (reg8 *) servo2__SIO_DIFF)
#endif /* (servo2__SIO_CFG) */

/* Interrupt Registers */
#if defined(servo2__INTSTAT)
    #define servo2_INTSTAT            (* (reg8 *) servo2__INTSTAT)
    #define servo2_SNAP               (* (reg8 *) servo2__SNAP)
    
	#define servo2_0_INTTYPE_REG 		(* (reg8 *) servo2__0__INTTYPE)
#endif /* (servo2__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_servo2_H */


/* [] END OF FILE */
