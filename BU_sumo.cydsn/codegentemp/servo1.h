/*******************************************************************************
* File Name: servo1.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_servo1_H) /* Pins servo1_H */
#define CY_PINS_servo1_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "servo1_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 servo1__PORT == 15 && ((servo1__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    servo1_Write(uint8 value);
void    servo1_SetDriveMode(uint8 mode);
uint8   servo1_ReadDataReg(void);
uint8   servo1_Read(void);
void    servo1_SetInterruptMode(uint16 position, uint16 mode);
uint8   servo1_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the servo1_SetDriveMode() function.
     *  @{
     */
        #define servo1_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define servo1_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define servo1_DM_RES_UP          PIN_DM_RES_UP
        #define servo1_DM_RES_DWN         PIN_DM_RES_DWN
        #define servo1_DM_OD_LO           PIN_DM_OD_LO
        #define servo1_DM_OD_HI           PIN_DM_OD_HI
        #define servo1_DM_STRONG          PIN_DM_STRONG
        #define servo1_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define servo1_MASK               servo1__MASK
#define servo1_SHIFT              servo1__SHIFT
#define servo1_WIDTH              1u

/* Interrupt constants */
#if defined(servo1__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in servo1_SetInterruptMode() function.
     *  @{
     */
        #define servo1_INTR_NONE      (uint16)(0x0000u)
        #define servo1_INTR_RISING    (uint16)(0x0001u)
        #define servo1_INTR_FALLING   (uint16)(0x0002u)
        #define servo1_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define servo1_INTR_MASK      (0x01u) 
#endif /* (servo1__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define servo1_PS                     (* (reg8 *) servo1__PS)
/* Data Register */
#define servo1_DR                     (* (reg8 *) servo1__DR)
/* Port Number */
#define servo1_PRT_NUM                (* (reg8 *) servo1__PRT) 
/* Connect to Analog Globals */                                                  
#define servo1_AG                     (* (reg8 *) servo1__AG)                       
/* Analog MUX bux enable */
#define servo1_AMUX                   (* (reg8 *) servo1__AMUX) 
/* Bidirectional Enable */                                                        
#define servo1_BIE                    (* (reg8 *) servo1__BIE)
/* Bit-mask for Aliased Register Access */
#define servo1_BIT_MASK               (* (reg8 *) servo1__BIT_MASK)
/* Bypass Enable */
#define servo1_BYP                    (* (reg8 *) servo1__BYP)
/* Port wide control signals */                                                   
#define servo1_CTL                    (* (reg8 *) servo1__CTL)
/* Drive Modes */
#define servo1_DM0                    (* (reg8 *) servo1__DM0) 
#define servo1_DM1                    (* (reg8 *) servo1__DM1)
#define servo1_DM2                    (* (reg8 *) servo1__DM2) 
/* Input Buffer Disable Override */
#define servo1_INP_DIS                (* (reg8 *) servo1__INP_DIS)
/* LCD Common or Segment Drive */
#define servo1_LCD_COM_SEG            (* (reg8 *) servo1__LCD_COM_SEG)
/* Enable Segment LCD */
#define servo1_LCD_EN                 (* (reg8 *) servo1__LCD_EN)
/* Slew Rate Control */
#define servo1_SLW                    (* (reg8 *) servo1__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define servo1_PRTDSI__CAPS_SEL       (* (reg8 *) servo1__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define servo1_PRTDSI__DBL_SYNC_IN    (* (reg8 *) servo1__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define servo1_PRTDSI__OE_SEL0        (* (reg8 *) servo1__PRTDSI__OE_SEL0) 
#define servo1_PRTDSI__OE_SEL1        (* (reg8 *) servo1__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define servo1_PRTDSI__OUT_SEL0       (* (reg8 *) servo1__PRTDSI__OUT_SEL0) 
#define servo1_PRTDSI__OUT_SEL1       (* (reg8 *) servo1__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define servo1_PRTDSI__SYNC_OUT       (* (reg8 *) servo1__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(servo1__SIO_CFG)
    #define servo1_SIO_HYST_EN        (* (reg8 *) servo1__SIO_HYST_EN)
    #define servo1_SIO_REG_HIFREQ     (* (reg8 *) servo1__SIO_REG_HIFREQ)
    #define servo1_SIO_CFG            (* (reg8 *) servo1__SIO_CFG)
    #define servo1_SIO_DIFF           (* (reg8 *) servo1__SIO_DIFF)
#endif /* (servo1__SIO_CFG) */

/* Interrupt Registers */
#if defined(servo1__INTSTAT)
    #define servo1_INTSTAT            (* (reg8 *) servo1__INTSTAT)
    #define servo1_SNAP               (* (reg8 *) servo1__SNAP)
    
	#define servo1_0_INTTYPE_REG 		(* (reg8 *) servo1__0__INTTYPE)
#endif /* (servo1__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_servo1_H */


/* [] END OF FILE */
