/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
//#include "tactic0.h"
#include <project.h>
/*
#include "tactic0.h"
#include "tactic1.h"
#include "tactic2.h"
#include "tactic3.h"
#include "tactic4.h"
#include "tactic5.h"
#include "tactic6.h"
#include "tactic7.h"
*/

#include "first_move.h"
#include "tactic0_basic.h"
#include "tactic1_Nooo.h"
#include "tactic2_blitz.h"
#include "tactic3_scary.h"
#include "tactic4_ND.h"
#include "tactic5_ND_blitz.h"
#include "tactic7.h"

//#include "tactic_test.h"
#include "stdint.h"
#include "menu.h"
#include "ssd1306.h"
#include "stdlib.h"


#define Rd_bits(value, mask)        ((value)&(mask))

#define TacticNumber 4

const char fightMenu_title[] = " #F";

const char fightMenu_1[] = "1.BASIC";
const char fightMenu_2[] = "2.Nooo";
const char fightMenu_3[] = "3.BLITZ";
const char fightMenu_4[] = "4.Scary";
const char fightMenu_5[] = "5.BLITZ R45";
const char fightMenu_6[] = "6.Right 45";
const char fightMenu_7[] = "7.Left 45";
const char fightMenu_8[] = "8.fast forward";
const char fightMenu_9[] = "9.nO Delay";
const char fightMenu_10[] = "10.ND Blitz";
const char fightMenu_11[] = "11.2xFastForwar";
const char fightMenu_12[] = "12.ForwardToLin";
const char fightMenu_13[] = "13.stupido";
const char fightMenu_14[] = "14.SlowToLin";
const char fightMenu_15[] = "15.SlowBasic";


const char* fightMenu_strings[] = {fightMenu_1, fightMenu_2, fightMenu_3, fightMenu_4, fightMenu_5, fightMenu_6, 
    fightMenu_7, fightMenu_8, fightMenu_9, fightMenu_10, fightMenu_11, fightMenu_12, fightMenu_13, fightMenu_14, fightMenu_15 };

void tacticSelector(){
    Menu FightMenu = {fightMenu_title, fightMenu_strings, 15, 0, 0};
    
		// (Re)Initialize menu system and wait for menu item selection.
    int tactic = ShowMenu(&FightMenu); 
    
    char text[5];
    itoa(tactic, text, 10);
      
        display_clear(); 
        gfx_setTextSize(2);
        gfx_setTextColor(WHITE);
        gfx_setTextBg(BLACK);
        gfx_setCursor(5,1);
        gfx_print("FIGHT"); 
        display_update();
        display_invert(1);
                   
        switch (tactic) {
            case 0:
            RunTactic0(DEFAULT, 75);
            break;
            case 1:
            RunTactic1();
            break;
            case 2:
            RunTactic2(DEFAULT);
            break;
            case 3:
            RunTactic3();
            break;
            case 4:
            RunTactic2(TURN_RIGHT_45);
            break;
            case 5:
            RunTactic0(TURN_RIGHT_45, 75);
            break;
            case 6:
            RunTactic0(TURN_LEFT_45, 75);
            break;
            case 7:
            RunTactic0(FAST_FORWARD, 75);
            break;
            case 8:
            RunTactic4();
            break;
            case 9:
            RunTactic5();
            break;
            case 10:
            RunTactic0(DOUBLE_FAST_FORWARD, 75);
            break;
            case 11:
            RunTactic0(DRIVE_TO_LINE, 75);
            break;
            case 12:
            RunTactic7(FAST_FORWARD);
            break;
            case 13:
            RunTactic0(DRIVE_SLOW_TO_LINE, 75);
            break;
            case 14:
            RunTactic0(DEFAULT, 35);
            break;
        } 
}
/* [] END OF FILE */
