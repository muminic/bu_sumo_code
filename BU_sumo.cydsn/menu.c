/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "ssd1306.h"
#include "menu.h"
#include "sensors.h"
#include <stdio.h>
#include <stdlib.h>
#include "button_input.h"

#define Rd_bits(value, mask)        ((value)&(mask))

static const unsigned int bitmap[] =
{
   0b00000000, 0b00000000, 0b00000011,
   0b00000000, 0b00000000, 0b00000011,
   0b00000000, 0b00000000, 0b00001110,
   0b00000000, 0b00000000, 0b00001100,
   0b00000000, 0b00000000, 0b00111100,
   0b00000000, 0b00000000, 0b11111100,
   0b00000000, 0b00000011, 0b11110000,
   0b00000000, 0b00000011, 0b11110000,
   0b00000000, 0b00001111, 0b11000000,
   0b00000000, 0b00001111, 0b11000000,
   0b00000000, 0b00111111, 0b00000000,
   0b00000000, 0b00111111, 0b00000000,
   0b00000000, 0b11111100, 0b00000000,
   0b00000000, 0b11111100, 0b00000000,
   0b00000011, 0b11110000, 0b00000000,
   0b00000011, 0b11110000, 0b00000000,
   0b00001111, 0b11000000, 0b00000000,
   0b00001111, 0b11000000, 0b00000000,
   0b00111111, 0b11111111, 0b11110000,
   0b00111111, 0b11111111, 0b11110000,
   0b11111111, 0b11111111, 0b11000000,
   0b11111111, 0b11111111, 0b11000000,
   0b00000000, 0b11111111, 0b00000000,
   0b00000000, 0b11111111, 0b00000000,
   0b00000000, 0b11111100, 0b00000000,
   0b00000000, 0b11111100, 0b00000000,
   0b00000011, 0b11110000, 0b00000000,
   0b00000011, 0b11110000, 0b00000000,
   0b00001111, 0b11000000, 0b00000000,
   0b00001111, 0b11000000, 0b00000000,
   0b00001111, 0b00000000, 0b00000000,
   0b00001111, 0b00000000, 0b00000000,
   0b00111100, 0b00000000, 0b00000000,
   0b00111100, 0b00000000, 0b00000000,
   0b00110000, 0b00000000, 0b00000000,
   0b01110000, 0b00000000, 0b00000000,
   0b11000000, 0b00000000, 0b00000000,
   0b11000000, 0b00000000, 0b00000000,
   0b00000000, 0b00000000, 0b00000000,
   0b00000000, 0b00000000, 0b00000000,
};


void showText(int x, char* text){
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setCursor(x,5);
    gfx_println(text);
    display_update();  
}

void mainLogo(void) {
    display_clear(); 
    gfx_setTextSize(2);
    gfx_setTextColor(WHITE);
    gfx_setCursor(35,25);
    gfx_println("ZIBSNIS");
    
    drawBitmap(5, 15, bitmap, 24, 40, 1);
    
    
    
    display_update();  
}

void menuDraw(Menu *menu)
{
    const char* title = menu->title;
	const char* *strings = menu->strings;
        
    char current_page = menu->current_selection/2 ;
    char cursor = menu->current_selection % 2;  
  
    
    char colorLine1 = 1;
    char colorLine2 = 1;
    //char colorLine3 = 1;
    //char colorLine4 = 1;
    switch (cursor) {
    case 0: 
    colorLine1 = 0;
     break;
    case 1: 
    colorLine2 = 0;
    break;
    //case 2: 
    //colorLine3 = 0;
    //break;
    //case 3: 
    //colorLine4 = 0;
    //break;

    }
        
        
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    gfx_setCursor(75,0);
    gfx_println(title);
       
    gfx_setCursor(0,0);
    gfx_setTextSize(1);
        
    gfx_setTextColor(colorLine1);
    gfx_setTextBg(!colorLine1);
    gfx_println(strings[current_page*2]);
        
    gfx_setTextColor(colorLine2);
    gfx_setTextBg(!colorLine2);
    gfx_println(strings[(current_page*2)+1]);
        
    //gfx_setTextColor(colorLine3);
    //gfx_setTextBg(!colorLine3);
    //gfx_println(strings[(current_page*2)+2]);
        
    //gfx_setTextColor(colorLine4);
    //gfx_setTextBg(!colorLine4);
    //gfx_println(strings[(current_page*2)+3]);
              
    display_update();   
}

updateMenuCursor(Menu *menu, int buttoncode)
{

	switch (buttoncode) {
	case MENU_KEYCODE_DOWN:
		if (menu->current_selection == menu->strings_cnt - 1) {
			menu->current_selection = 0;
		} else {
			menu->current_selection++;
		}

		/* Update menu on display */
		menuDraw(menu);
		/* Nothing selected yet */
		return MENU_EVENT_IDLE;

	case MENU_KEYCODE_UP:
		if (menu->current_selection) {
			menu->current_selection--;
		} else {
			menu->current_selection = menu->strings_cnt - 1;
		}

		/* Update menu on display */
		menuDraw(menu);
		/* Nothing selected yet */
		return MENU_EVENT_IDLE;

	case MENU_KEYCODE_ENTER:
		/* Got what we want. Return selection. */
		return menu->current_selection;
	default:
		/* Unknown key event */
		return MENU_EVENT_IDLE;
    }
}


int ShowMenu(Menu *menu){
    keyboard_event_t input;
    
    int menuStatus;
    menuDraw(menu);
    do {
	    do {
			GetButtonState(&input);
			// Wait for button release
		} while( KEYACTION_RELEASE != input.buttonaction );

        
		menuStatus = updateMenuCursor(menu, input.buttoncode);

	} while( MENU_EVENT_IDLE == menuStatus );
    
    return menuStatus;
}


void showNumber(int number) {
    char text[5];
    itoa(number, text, 10);
    display_clear(); 
    gfx_setTextSize(1);
    gfx_setTextColor(WHITE);
    gfx_setTextBg(BLACK);
    gfx_setCursor(25,0);
    gfx_print(text);  
    display_update();
}

   

/* [] END OF FILE */
